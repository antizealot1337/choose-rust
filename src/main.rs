use clap::{Arg, ArgAction, Command};
use std::{borrow::Cow, process::ExitCode};

const ARG_OPTIONS_ID: &'static str = "options";
const ARG_INTERACTIVE_ID: &'static str = "interactive";
const ARG_ALLOW_NON_PRINTABLE: &'static str = "printable";

fn main() -> ExitCode {
    let matches = Command::new("Choose")
        .about("Make a choice")
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .arg(
            Arg::new(ARG_INTERACTIVE_ID)
                .long("interactive")
                .short('i')
                .help("Read options from the command line")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new(ARG_OPTIONS_ID)
                .help("The options from which to choose")
                .action(ArgAction::Append),
        )
        .arg(
            Arg::new(ARG_ALLOW_NON_PRINTABLE)
                .long("allow-non-printable")
                .short('p')
                .help("Check the options to make sure they contain only printable characters")
                .default_value("false")
                .value_parser(clap::value_parser!(bool))
                .action(ArgAction::Set),
        )
        .get_matches();

    let options: Vec<Cow<str>> = match matches.get_many::<String>(ARG_OPTIONS_ID) {
        Some(options) => {
            let interactive = *matches
                .get_one::<bool>(ARG_INTERACTIVE_ID)
                .expect("failed to get interactive flag");
            let mut options: Vec<Cow<str>> = options.map(|s| s.into()).into_iter().collect();

            if interactive {
                read_from_stdin(&mut options).expect("failed to read from stdin");
            }

            options
        }
        None => {
            let mut options: Vec<Cow<str>> = Vec::new();

            read_from_stdin(&mut options).expect("failed to read from stdin");

            options
        }
    };

    let allow_non_printable = matches.get_flag(ARG_ALLOW_NON_PRINTABLE);

    if !allow_non_printable {
        for option in &options {
            if contains_non_printable_characters(option) {
                bunt::eprintln!("{$red+bold}error{/$}: non printable characters detected");
                return ExitCode::FAILURE;
            }
        }
    }

    let choice = match options.len() {
        0 => {
            bunt::eprintln!("{$red+bold}error{/$}: no choices provided");
            return ExitCode::FAILURE;
        }
        1 => &options[0],
        _ => &options[fastrand::usize(0..options.len())],
    };

    println!("{choice}");

    ExitCode::SUCCESS
}

fn read_from_stdin(options: &mut Vec<Cow<str>>) -> std::io::Result<()> {
    let stdin = std::io::stdin();

    'reading: loop {
        let mut line = String::new();

        let line = match stdin.read_line(&mut line)? {
            0 => break 'reading,
            1 => continue,
            _ => {
                let line = line.trim();

                if line.is_empty() {
                    continue;
                }

                line.to_owned()
            }
        };

        options.push(line.into());
    }

    Ok(())
}

fn contains_non_printable_characters(s: &Cow<str>) -> bool {
    for char in s.chars() {
        if char.is_control() {
            return true;
        }
    }

    false
}

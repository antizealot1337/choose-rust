# choose

A program to help the indecisive.

## usage

There are 2 main ways to use this program to select a random options.
Options can be provided one line at a time to the program via `STDIN`.

```sh
$ cat > choices.txt << EOF
option1
option2
option3
EOF
$ choose < choices.txt
option1
```

Another way to randomly select a choice is to provide them directly to the
program via arguments.

```sh
$ choose option1 option2 option3
option1
```

Reading from `STDIN` is the default mode if arguments are not provided. If
arguments are provded and reading from `STDIN` is still desired then use the
`--interactive`/`-i` flag.

Options containing leading or trailing space are automatically will have the
spaces removed.

Any options that have any Unicode control characters will cause the program to
terminate in error. To prevent the check use the `--allow-non-printable`/`-p`
flag.

## license

Copyright under the terms of the MIT license. Please refer to the LICENSE file
for more information.
